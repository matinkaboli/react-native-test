/*
  yarn add react-native-material-ui
  yarn add react-native-vector-icons
  react-native link react-native-vector-icons
*/

import React, { Component } from 'react';
import { View, Text, Navigator, NativeModules } from 'react-native';
import { COLOR, ThemeContext, getTheme, Button, Checkbox } from 'react-native-material-ui';

const uiTheme = {
  toolbar: {
    container: {
      height: 50,
    },
  },
};

class A extends Component {
  state = {
    checked: true,
  };

  render() {
    return (
      <View style={{
        margin: 20,
      }}>
        <Text>Hello</Text>

        <Button
          primary
          raised
          text="Primary"
        />
      </View>
    );
  }
}

export default class App extends Component {
  render() {
    return (
      <ThemeContext.Provider value={getTheme(uiTheme)}>
        <A />
      </ThemeContext.Provider>
    );
  }
}
